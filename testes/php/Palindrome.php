<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.

*/




class Palindrome
{
	public static function isPalindrome($word)
	{

		$inverse =  strrev(strtoupper($word));  // Utilizamos o strtoupper para que não haja interferência de caracteres maiúsculos ou minúsculos

		$string_reverse = str_split($inverse); 

		$palindrome = ''; 

		foreach($string_reverse as $value){

			$palindrome.= $value; 
		}

		if(strtoupper($word) == $palindrome){ // Utilizamos o strtoupper para que não haja interferência de caracteres maiúsculos ou minúsculos
			
            return true;

		} else {

			return false;

		}
	}
}

// Ou de forma mais simples


class Palindrome
{
	public static function isPalindrome($word)
	{

		$inverse =  strrev(strtoupper($word)); 

		if(strtoupper($word) == $inverse){     // Utilizamos o strtoupper para que não haja interferência de caracteres maiúsculos ou minúsculos
			return true;

		} else {

			return false;

		}
	}
}

echo Palindrome::isPalindrome('ovo');

echo Palindrome::isPalindrome('Deleveled');