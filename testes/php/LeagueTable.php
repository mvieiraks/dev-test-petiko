<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizando a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a função playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
	public function __construct($players)
	{
		$this->standings = array();
		foreach($players as $index => $p)
		{
			$this->standings[$p] = array
			(
				'index' => $index,
				'games_played' => 0, 
				'score' => 0
			);
		}
	}

	public function recordResult($player, $score)
	{
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}
	
	public function playerRank($rank)
	{


		$ranking = $this->standings;

		uasort($ranking, function ($a, $b) {  // Inicializamos a ordenação e definimos a função de comparação
        
			if ($a['score'] != $b['score']) {
				return ($a['score'] > $b['score']) ? -1 : 1; // Comparamos primeiramente por pontuação
			}
 
			if ($a['games_played'] != $b['games_played']) {
				return ($a['games_played'] < $b['games_played']) ? -1 : 1;  // Comparamos por quantidade de jogos
			}
     
			return ($a['index'] < $b['index']) ? -1 : 1; // Comparamos a posição na lista 
		});

		return array_keys($ranking)[$rank-1]; // Precisamos do primeiro valor, porém arrays começam em 0, por isso subtraímos 1 do rank desejado

	}
}

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);

// Esse foi o principal motivo da demora na entrega, alguns dias quebrando a cabeça e pesquisando diferentes métodos até aprender como esse problema poderia ser resolvido, postura e esforço que vou buscar aplicar todos os dias na petiko.

?>