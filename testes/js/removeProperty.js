// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  
  if (prop in obj) { // Checamos se existe a propriedade prop dentro do objeto obj
    delete obj[prop]; // Deletamos a propriedade
    return true; // Retornamos verdadeiro
  } else {
    return false; // Retornamos falso
  }

  // Outra opção utilizando o método hasOwnProperty

  if (obj.hasOwnProperty(prop)) { // Checamos se existe a propriedade prop dentro do objeto obj
    delete obj[prop]; // Deletamos a propriedade
    return true; // Retornamos verdadeiro
  } else {
    return false; // Retornamos falso
  }
  
}