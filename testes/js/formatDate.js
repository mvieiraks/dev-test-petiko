// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
    var date = new Date(userDate), // Colocamos o valor recebido em "userDate" dentro da variável date
        month = '' + (date.getMonth() + 1), // Colocamos o mês dentro da varável month e adicionamos +1 por getMonth ser zero-based
        day = '' + date.getDate(), // Colocamos o dia dentro da variável day
        year = date.getFullYear(); // Colocamos o ano da variável year

    if (month.length < 2) month = '0' + month; // Verificamos se o mês tem menos de dois caracteres, caso positivo adicionamos um zero
    if (day.length < 2) day = '0' + day; // Verificamos se o dia tem menos de dois caracteres, caso positivo adicionamos um zero

    return [year+month+day]; // Retornamos o valor no formato exigido pela API YYYYMMDD
}

// No formato MM/DD/YYYY para YYYYMMDD podemos utilizar também, apenas reposicionando os valores da string e retirando os caracteres especiais

function formatDate(userDate) {

 var date = new Date(userDate);
 var res = date.toISOString().slice(0,10).replace(/-/g,"");
 
 return [res];
 
}


console.log(formatDate("12/31/2014"));